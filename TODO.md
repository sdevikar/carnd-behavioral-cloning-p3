**What we have:**
- drive.py: Use when you have trained your model to drive the car.
- video.py: Use this to make a video
- writeup_template.md: Write your report following this template
- Simulator: Has two tracks
- Sample driving data for the first track. Can be optionally used to train the network

**Collecting training data from Simulator**
- Use mouse to drive. This gives better steering angles

| Task                          | How to do it                                  |
| :---------------------------- | :-------------------------------------------- |
| Steer left                    | Left mouse button AND drag mouse left         |
| Steer right                   | Right mouse button AND drag mouse right       |
| Record                        | Press R or press the record button            |
| Take over in Autonomous mode  | Press W or S (releasing to resume Auto mode   |
| Toggle cruise control         | Spacebar                                      |
| Brake                         | Back arrow                                    |

- Start recording and drive around a few laps. (stop recording and save recording)
  - Try to drive with different styles
  - Try to wander off every once in a while and recover
  - if while collecting the data, car is about to fall into a ditch or goes out of control, stop recording to prevent from driving n laps all over again.
  -
- Recording contains:
  - IMG folder: All frames
  - driving_log.csv: correlates images with steering angle, throttle, brake and speed
  - The format for csv is as follows:

| Center Image| Left Image  | Right Image | Steering Angle| Throttle  | Brake | Speed |
| :-----------| :-----------| :-----------| :-----------  | :---------| :-----| :-----
| xxx         | yyy         | zzz         | 0.123         | 0.894     | 0     | g     |


**Training process**
- **Input:**: Take an image from the center camera of the car
  - What is this center image?
- **Output**: Output a new steering angle for this Input
- Save the trained model as model.h5 using
  - ```
  model.save('model.h5').
  ```
  - Read [this](https://keras.io/getting-started/faq/#how-can-i-save-a-keras-model) for more details
- **Validation**
  - Use the data that was not used for training. You can do this with the same data you collected by driving around and splitting the data
- **Testing**: Can do this by launching the Simulator in autonomous mode
  - Run the server using:
  ```
  python drive.py model.h5
  ```
  - Until the server is run, the car will just sit there and do nothing

**Tips and tricks**
- If training and validation accuracy both are bad, that means you are underfitting. The fix for this is to:
  - Provide more epochs
  - add more convolution to the network

- If training accuracy is good and validation accuracy is bad, that means you're overfitting. The fix for this is to:
  - Add dropout or pooling layers
  - user fewer convolutions or use fewer fully connected layers
  - Collect more data
- Ideally, the model will make good predictions on both the training and validation sets.
- If your model has low mean squared error on the training and validation sets but is driving off the track, this could be because of the data collection process.
- It's important to feed the network examples of good driving behavior so that the vehicle stays in the center and recovers when getting too close to the sides of the road.
- **Teaching car how to recover is important** because, even with a lot of training data, chances are the car will drive off the road and won't know how to recover. But this doesn't mean one should constantly wander off the road and recover, because then, that's what the car will do while driving in autonomous mode.
- It is for the ease of recovery, that three cameras are provided in real car (not in this simulator). This is smart because For example,
  - if you train the model to associate a given image from the center camera with a left turn, then
  - you could also train the model to associate the corresponding image from the left camera with a somewhat softer left turn.
  - And you could train the model to associate the corresponding image from the right camera with an even harder left turn.




**TODO**
- Build model using keras
- Use sample data to train the model
- test and see how it performs
- Drive the vehicle, collect data
- Train the model again
